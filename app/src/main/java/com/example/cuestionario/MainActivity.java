package com.example.cuestionario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    //private CheckBox checkMID;
    //private CheckBox checkTOP;
    //// private CheckBox checkADC;
    private CheckBox checkJG;
    // private CheckBox checkSUP;
    private RadioButton radioBlue;
    //private RadioButton radioRed;
    private Switch switchPregunta4;
    private ToggleButton togglePregunta5;
    //private RadioButton radioHeraldo;
    private RadioButton radioBaron;
    //private RadioButton radioDrake;
    //private RadioButton radioJonia;
    private RadioButton radioDemacia;
    private RadioButton radioJonia2;
    //private RadioButton radioDemacia2;

    //private RadioButton radioPregunta9_1;
    private RadioButton radioPregunta9_2;
    //private RadioButton radioPregunta9_3;
    //private RadioButton radioPregunta9_4;
    //private RadioButton radioPregunta9_5;

    //private RadioButton radioPregunta10_1;
    //private RadioButton radioPregunta10_2;
    //private RadioButton radioPregunta10_3;
    private RadioButton radioPregunta10_4;
    //private RadioButton radioPregunta10_5;

    //private RadioButton radioPregunta11_1;
    //private RadioButton radioPregunta11_2;
    private RadioButton radioPregunta11_3;
    //private RadioButton radioPregunta11_4;
    //private RadioButton radioPregunta11_5;

    //private RadioButton radioPregunta12_1;
    //private RadioButton radioPregunta12_2;
    //private RadioButton radioPregunta12_3;
    //private RadioButton radioPregunta12_4;
    private RadioButton radioPregunta12_5;

    //private RadioButton radioPregunta13_1;
    //private RadioButton radioPregunta13_2;
    //private RadioButton radioPregunta13_3;
    private RadioButton radioPregunta13_4;
    // private RadioButton radioPregunta13_5;

    private RadioButton radioPregunta14_1;
    //private RadioButton radioPregunta14_2;
    //private RadioButton radioPregunta14_3;
    //private RadioButton radioPregunta14_4;
    //private RadioButton radioPregunta14_5;

    //private RadioButton radioPregunta15_1;
    //private RadioButton radioPregunta15_2;
    //private RadioButton radioPregunta15_3;
    private RadioButton radioPregunta15_4;
    //private RadioButton radioPregunta15_5;

    //private RadioButton radioPregunta16_1;
    private RadioButton radioPregunta16_2;
    //private RadioButton radioPregunta16_3;
    //private RadioButton radioPregunta16_4;
    //private RadioButton radioPregunta16_5;

    //private RadioButton radioPregunta17_1;
    //private RadioButton radioPregunta17_2;
    //private RadioButton radioPregunta17_3;
    private RadioButton radioPregunta17_4;
    //private RadioButton radioPregunta17_5;

    //private RadioButton radioPregunta18_1;
    private RadioButton radioPregunta18_2;
    //private RadioButton radioPregunta18_3;
    //private RadioButton radioPregunta18_4;
    //private RadioButton radioPregunta18_5;

    //private RadioButton radioPregunta19_1;
    private RadioButton radioPregunta19_2;
    //private RadioButton radioPregunta19_3;
    //private RadioButton radioPregunta19_4;
    //private RadioButton radioPregunta19_5;

    //private RadioButton radioPregunta20_1;
    //private RadioButton radioPregunta20_2;
    //private RadioButton radioPregunta20_3;
    private RadioButton radioPregunta20_4;
    //private RadioButton radioPregunta20_5;
    private TextView txtTotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // this.checkADC = findViewById(R.id.checkADC);
        this.checkJG = findViewById(R.id.checkJG);
        // this.checkMID = findViewById(R.id.checkMID);
        // this.checkSUP = findViewById(R.id.checkSUP);
        //  this.checkTOP = findViewById(R.id.checkTOP);
        this.radioBaron = findViewById(R.id.radioBaron);
        this.radioBlue = findViewById(R.id.radioblue);
        this.radioDemacia = findViewById(R.id.radioDemacia);
        // this.radioRed = findViewById(R.id.radiored);
        this.switchPregunta4 = findViewById(R.id.switchPregunta4);
        this.togglePregunta5 = findViewById(R.id.togglePregunta5);
        //  this.radioHeraldo = findViewById(R.id.radioHeraldo);
        // this.radioDrake = findViewById(R.id.radioDragon);
        // this.radioJonia = findViewById(R.id.radioJonia);
        this.radioJonia2 = findViewById(R.id.radioJonia2);
        //this.radioDemacia2 = findViewById(R.id.radioDemacia2);
        // this.radioPregunta9_1 = findViewById(R.id.radioPregunta9_1);
        this.radioPregunta9_2 = findViewById(R.id.radioPregunta9_2);
        // this.radioPregunta9_3 = findViewById(R.id.radioPregunta9_3);
        //  this.radioPregunta9_4 = findViewById(R.id.radioPregunta9_4);
        //  this.radioPregunta9_5 = findViewById(R.id.radioPregunta9_5);

        // this.radioPregunta10_1 = findViewById(R.id.radioPregunta10_1);
        // this.radioPregunta10_2 = findViewById(R.id.radioPregunta10_2);
        //   this.radioPregunta10_3 = findViewById(R.id.radioPregunta10_3);
        this.radioPregunta10_4 = findViewById(R.id.radioPregunta10_4);
        //  this.radioPregunta10_5 = findViewById(R.id.radioPregunta10_5);

        //  this.radioPregunta11_1 = findViewById(R.id.radioPregunta11_1);
        //   this.radioPregunta11_2 = findViewById(R.id.radioPregunta11_2);
        this.radioPregunta11_3 = findViewById(R.id.radioPregunta11_3);
        //   this.radioPregunta11_4 = findViewById(R.id.radioPregunta11_4);
        // this.radioPregunta11_5 = findViewById(R.id.radioPregunta11_5);

        // this.radioPregunta12_1 = findViewById(R.id.radioPregunta12_1);
        // this.radioPregunta12_2 = findViewById(R.id.radioPregunta12_2);
        // this.radioPregunta12_3 = findViewById(R.id.radioPregunta12_3);
        // this.radioPregunta12_4 = findViewById(R.id.radioPregunta12_4);
        this.radioPregunta12_5 = findViewById(R.id.radioPregunta12_5);

        //  this.radioPregunta13_1 = findViewById(R.id.radioPregunta13_1);
        //  this.radioPregunta13_2 = findViewById(R.id.radioPregunta13_2);
        // this.radioPregunta13_3 = findViewById(R.id.radioPregunta13_3);
        this.radioPregunta13_4 = findViewById(R.id.radioPregunta13_4);
        // this.radioPregunta13_5 = findViewById(R.id.radioPregunta13_5);

        this.radioPregunta14_1 = findViewById(R.id.radioPregunta14_1);
        //this.radioPregunta14_2 = findViewById(R.id.radioPregunta14_2);
        //  this.radioPregunta14_3 = findViewById(R.id.radioPregunta14_3);
        //  this.radioPregunta14_4 = findViewById(R.id.radioPregunta14_4);
        //  this.radioPregunta14_5 = findViewById(R.id.radioPregunta14_5);

        // this.radioPregunta15_1 = findViewById(R.id.radioPregunta15_1);
        // this.radioPregunta15_2 = findViewById(R.id.radioPregunta15_2);
        // this.radioPregunta15_3 = findViewById(R.id.radioPregunta15_3);
        this.radioPregunta15_4 = findViewById(R.id.radioPregunta15_4);
        // this.radioPregunta15_5 = findViewById(R.id.radioPregunta15_5);

        //  this.radioPregunta16_1 = findViewById(R.id.radioPregunta16_1);
        this.radioPregunta16_2 = findViewById(R.id.radioPregunta16_2);
        // this.radioPregunta16_3 = findViewById(R.id.radioPregunta16_3);
        //  this.radioPregunta16_4 = findViewById(R.id.radioPregunta16_4);
        // this.radioPregunta16_5 = findViewById(R.id.radioPregunta16_5);

        // this.radioPregunta17_1 = findViewById(R.id.radioPregunta17_1);
        // this.radioPregunta17_2 = findViewById(R.id.radioPregunta17_2);
        // this.radioPregunta17_3 = findViewById(R.id.radioPregunta17_3);
        this.radioPregunta17_4 = findViewById(R.id.radioPregunta17_4);
        // this.radioPregunta17_5 = findViewById(R.id.radioPregunta17_5);

        //  this.radioPregunta18_1 = findViewById(R.id.radioPregunta18_1);
        this.radioPregunta18_2 = findViewById(R.id.radioPregunta18_2);
        //   this.radioPregunta18_3 = findViewById(R.id.radioPregunta18_3);
        //  this.radioPregunta18_4 = findViewById(R.id.radioPregunta18_4);
        //  this.radioPregunta18_5 = findViewById(R.id.radioPregunta18_5);

        //  this.radioPregunta19_1 = findViewById(R.id.radioPregunta19_1);
        this.radioPregunta19_2 = findViewById(R.id.radioPregunta19_2);
        // this.radioPregunta19_3 = findViewById(R.id.radioPregunta19_3);
        // this.radioPregunta19_4 = findViewById(R.id.radioPregunta19_4);
        // this.radioPregunta19_5 = findViewById(R.id.radioPregunta19_5);

        //  this.radioPregunta20_1 = findViewById(R.id.radioPregunta20_1);
        //  this.radioPregunta20_2 = findViewById(R.id.radioPregunta20_2);
        //  this.radioPregunta20_3 = findViewById(R.id.radioPregunta20_3);
        this.radioPregunta20_4 = findViewById(R.id.radioPregunta20_4);
        // this.radioPregunta20_5 = findViewById(R.id.radioPregunta20_5);

        this.txtTotal = findViewById(R.id.txtTotal);

        checkJG.setOnCheckedChangeListener(this);
        radioBlue.setOnCheckedChangeListener(this);
        switchPregunta4.setOnCheckedChangeListener(this);
        togglePregunta5.setOnCheckedChangeListener(this);
        radioBaron.setOnCheckedChangeListener(this);
        radioDemacia.setOnCheckedChangeListener(this);
        radioJonia2.setOnCheckedChangeListener(this);
        radioPregunta9_2.setOnCheckedChangeListener(this);
        radioPregunta10_4.setOnCheckedChangeListener(this);
        radioPregunta11_3.setOnCheckedChangeListener(this);
        radioPregunta12_5.setOnCheckedChangeListener(this);
        radioPregunta13_4.setOnCheckedChangeListener(this);
        radioPregunta14_1.setOnCheckedChangeListener(this);
        radioPregunta15_4.setOnCheckedChangeListener(this);
        radioPregunta16_2.setOnCheckedChangeListener(this);
        radioPregunta17_4.setOnCheckedChangeListener(this);
        radioPregunta18_2.setOnCheckedChangeListener(this);
        radioPregunta19_2.setOnCheckedChangeListener(this);
        radioPregunta20_4.setOnCheckedChangeListener(this);

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int var = 0, suma = 0;
        String resp;
        var += checkJG.isChecked() ? 1 : 0;
        var += radioBlue.isChecked() ? 1 : 0;
        var += switchPregunta4.isChecked() ? 1 : 0;
        var += togglePregunta5.isChecked() ? 1 : 0;
        var += radioBaron.isChecked() ? 1 : 0 ;
        var += radioDemacia.isChecked() ? 1 : 0;
        var += radioJonia2.isChecked() ? 1 : 0;
        var += radioPregunta9_2.isChecked() ? 1 : 0;
        var += radioPregunta10_4.isChecked() ? 1 : 0;
        var += radioPregunta11_3.isChecked() ? 1 : 0;
        var += radioPregunta12_5.isChecked() ? 1 : 0;
        var += radioPregunta13_4.isChecked() ? 1 : 0;
        var += radioPregunta14_1.isChecked() ? 1 : 0;
        var += radioPregunta15_4.isChecked() ? 1 : 0;
        var += radioPregunta16_2.isChecked() ? 1 : 0;
        var += radioPregunta17_4.isChecked() ? 1 : 0;
        var += radioPregunta18_2.isChecked() ? 1 : 0;
        var += radioPregunta19_2.isChecked() ? 1 : 0;
        var += radioPregunta20_4.isChecked() ? 1 : 0;
        suma = suma + var;
        resp = String.valueOf(suma);
        txtTotal.setText("Resultados: " + resp);
    }
    public void calcular(View View)
    {
        txtTotal.setVisibility(android.view.View.VISIBLE);

    }
}